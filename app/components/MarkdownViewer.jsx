const CommonMark = require("commonmark");
const reader = new CommonMark.Parser();
const html = new CommonMark.HtmlRenderer();

export default function MarkdownViewer({ input }) {
  if (!input) return null;

  const markdown = reader.parse(input);
  const htmlString = html.render(markdown);
  return <div dangerouslySetInnerHTML={{ __html: htmlString }} />;
}
