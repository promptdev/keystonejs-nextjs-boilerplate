import Link from "next/link";

import CONFIG from "../config";

export default function Nav(props) {
  return (
    <nav>
      <ul>
        <li>
          <Link href="/">
            <a>{CONFIG.SLUGS.HOMEPAGE}</a>
          </Link>
        </li>
        {props.navItems &&
          props.navItems.map((item) => {
            // skips SLUGS.HOMEPAGE
            if (item.slug === CONFIG.SLUGS.HOMEPAGE) return;

            return (
              <li key={item.slug}>
                <Link href={item.slug}>
                  <a>{item.title}</a>
                </Link>
              </li>
            );
          })}
      </ul>
    </nav>
  );
}
