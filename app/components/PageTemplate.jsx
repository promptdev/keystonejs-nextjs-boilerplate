export default function PageTemplate({ header, nav, content }) {
  return (
    <main>
      {header && <header>{header}</header>}
      <div>
        {nav && <aside>{nav}</aside>}
        {content && <div>{content}</div>}
      </div>
    </main>
  );
}
