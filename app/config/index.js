const environment = process.env.NODE_ENV;
const isProduction = environment === "production";

module.exports = {
  DOMAIN: isProduction ? "https://angelmeraz.info" : "http://localhost:3000",

  SLUGS: {
    HOMEPAGE: "start",
  },
};
