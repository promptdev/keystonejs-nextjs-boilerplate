import Router from "next/router";

import CONFIG from "../config";
import MarkdownViewer from "../components/MarkdownViewer";
import Nav from "../components/Nav";
import PageTemplate from "../components/PageTemplate";

export default function Page({ navItemsData, pageData }) {
  if (Object.keys(pageData).length === 0) {
    typeof window !== "undefined" && Router.push("/404");
  }

  return (
    <PageTemplate
      nav={<Nav navItems={navItemsData} />}
      content={<MarkdownViewer input={pageData.body} />}
    />
  );
}

export async function getServerSideProps({ res, query }) {
  const request = await fetch(`${CONFIG.DOMAIN}/admin/api`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `{
                    allPages {
                        body
                        id
                        isMenuItem
                        slug
                        status
                        title
                        leadMedia {
                            path
                            publicUrl
                            filename
                        }
                    }
                }`,
    }),
  });
  const response = await request.json();

  // exit early: redirect to 404 if no response data
  if (res !== "undefined" && !response.data) {
    res.writeHead(301, {
      Location: "/404",
    });
    res.end();

    return {};
  }

  let navItemsData = [],
    queriedPageData = {};

  response.data.allPages?.forEach((page) => {
    // navItemsData
    if (page.status === "published" && page.isMenuItem) {
      navItemsData.push(page);
    }

    // queriedPageData
    if (page.status === "published" && page.slug === query.slug) {
      queriedPageData = { ...page };
    }
  });

  return {
    props: {
      navItemsData,
      pageData: queriedPageData,
    },
  };
}
