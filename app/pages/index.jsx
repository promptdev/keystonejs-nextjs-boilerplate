import CONFIG from "../config";
import MarkdownViewer from "../components/MarkdownViewer";
import Nav from "../components/Nav";
import PageTemplate from "../components/PageTemplate";

export default function Homepage({ navItemsData, pageData }) {
  return (
    <PageTemplate
      nav={<Nav navItems={navItemsData} />}
      content={<MarkdownViewer input={pageData.body} />}
    />
  );
}

export async function getServerSideProps({ res }) {
  const requestUri = `${CONFIG.DOMAIN}/admin/api`;
  const request = await fetch(requestUri, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `{
                    allPages {
                        body
                        id
                        isMenuItem
                        slug
                        status
                        title
                        leadMedia {
                            path
                            publicUrl
                            filename
                        }
                    }
                }`,
    }),
  });
  const response = await request.json();

  // exit early: redirect to 404 if no response data
  if (res !== "undefined" && !response.data) {
    res.writeHead(301, {
      Location: "/404",
    });
    res.end();

    return {};
  }

  let navItemsData = [],
    homepageData = {};

  response.data.allPages?.forEach((page) => {
    // navItemsData
    if (page.status === "published" && page.isMenuItem) {
      navItemsData.push(page);
    }

    // homepageData
    if (page.status === "published" && page.slug === CONFIG.SLUGS.HOMEPAGE) {
      homepageData = { ...page };
    }
  });

  return {
    props: {
      navItemsData,
      pageData: homepageData,
    },
  };
}
