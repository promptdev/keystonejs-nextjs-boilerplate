const environment = process.env.NODE_ENV;
const isProduction = environment === "production";

module.exports = {
  DATABASE_URI: isProduction ?
    "mongodb://localhost/keystone-boilerplate"
    : "mongodb://localhost/keystone-boilerplate",

  PROJECT_NAME: "keystone-boilerplate",
};
