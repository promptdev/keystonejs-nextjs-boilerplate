// Keystone main instance
const { Keystone } = require("@keystonejs/keystone");
const initialiseData = require("./config/InitialData");

// Main dependencies
const { AdminUIApp } = require("@keystonejs/app-admin-ui");
const { GraphQLApp } = require("@keystonejs/app-graphql");
const { NextApp } = require("@keystonejs/app-next");

// Project Config
const CONFIG = require("./config");

// Mongo Setup
const { MongooseAdapter: Adapter } = require("@keystonejs/adapter-mongoose");
const adapterConfig = {
  mongoUri: CONFIG.DATABASE_URI,
};

// exports Keystone instance
const keystone = new Keystone({
  adapter: new Adapter(adapterConfig),
  onConnect: process.env.CREATE_TABLES !== "true" && initialiseData,
  cookieSecret:
    "kp*?OFi#,*6w4<&L6#_EnP5>V8g6Xo{8dl?-]b4QE+7B>bc9$g|FA!X>ri/C&fA",
});

// Schemas to Lists
const Page = require("./lists/Page");
keystone.createList("Page", Page);

const User = require("./lists/User");
keystone.createList("User", User);

// Authentication
const { PasswordAuthStrategy } = require("@keystonejs/auth-password");
const authStrategy = keystone.createAuthStrategy({
  type: PasswordAuthStrategy,
  list: "User",
});

module.exports = {
  keystone,
  apps: [
    new GraphQLApp(),
    new AdminUIApp({
      name: CONFIG.PROJECT_NAME,
      authStrategy,
    }),
    new NextApp({
      dir: "app",
    }),
  ],
};
