const { Checkbox, File, Text, Select, Slug } = require("@keystonejs/fields");
const { LocalFileAdapter } = require("@keystonejs/file-adapters");
const { Wysiwyg } = require("@keystonejs/fields-wysiwyg-tinymce");
const {
  AuthedRelationship,
} = require("@keystonejs/fields-authed-relationship");
const { Content } = require("@keystonejs/fields-content");

const fileAdapter = new LocalFileAdapter({
  src: "./app/public/files",
  path: "/files",
});

module.exports = {
  fields: {
    title: { type: Text },
    slug: {
      from: "title",
      type: Slug,
      isUnique: true,
    },
    isMenuItem: {
      type: Checkbox,
      label: "Display in menu",
    },
    status: {
      type: Select,
      defaultValue: "draft",
      options: [
        { label: "Draft", value: "draft" },
        { label: "Published", value: "published" },
      ],
    },
    author: {
      type: AuthedRelationship,
      ref: "User",
      isRequired: true,
    },
    leadMedia: {
      type: File,
      adapter: fileAdapter,
    },
    body: {
      type: Text,
      isMultiline: true,
    },
  },
  labelResolver: (item) => item.title,
};
